import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';

export default class Acl {
  public async handle ({ auth, response }: HttpContextContract, next: () => Promise<void>, allowedRoles) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    let role = auth.user!.role
    if (allowedRoles.includes(role)){
      await next()
    } else {
      response.unauthorized({status: 'access denied!', message: 'you are unauthorized to make changes'})
    }
  }
}
