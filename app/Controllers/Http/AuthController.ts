import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import User from 'App/Models/User';
import RegisterValidator from 'App/Validators/RegisterValidator';
import LoginValidator from 'App/Validators/LoginValidator';
import Mail from '@ioc:Adonis/Addons/Mail';
import Database from '@ioc:Adonis/Lucid/Database';

export default class AuthController {
 /**
  * @swagger
  * /api/v1/register:
  *   post:
  *     tags:
  *       - Authentication
  *     summary: Registrasi user baru
  *     requestBody:
  *         required: true
  *         content:
  *             application/x-www-form-urlencoded:
  *                 schema:
  *                     $ref: '#definitions/User'
  *             application/json:
  *                 schema:
  *                     $ref: '#definitions/User'
  *     
  *     responses:
  *          200:
  *              description: 'Registration Successful!'
  *         
  */
    public async register({ request, response }: HttpContextContract){
        const payload = await request.validate(RegisterValidator)
        const newUser = await User.create({
            name: payload.name,
            email: payload.email,
            password: payload.password,
            // password_confirmation: payload.password,
            role: payload.role 
        })
        let otp_code: number = Math.floor(100000 + Math.random() * 900000)
        await Database.table('otps').insert({otp_code: otp_code, user_id: newUser.id})
        await Mail.send((message) =>{
            message
                .from('admin@sanberdev.com')
                .to(payload.email)
                .subject('Welcome')
                .htmlView('mail/otp', { name: payload.name, otp_code: otp_code })
        })
        return response.created({status: 'Registration Successful!', data: newUser})
    }
  /**
   * @swagger
   * paths:
   *  /api/v1/login:
   *    post:
   *      tags:
   *        - Authentication
   *      description: Validating user and assigning token
   *      summary: User login
   *      requestBody:
   *          required: true
   *          content:
   *              application/x-www-form-urlencoded:
   *                  schema:
   *                     type: object
   *                     properties:
   *                         email:
   *                           type: string
   *                         password:
   *                           type: string
   *                     required:
   *                        - email
   *                        - password
   *              application/json:
   *                  schema:
   *                     type: object
   *                     properties:
   *                         email:
   *                           type: string
   *                         password:
   *                           type: string
   *                     required:
   *                        - email
   *                        - password
   *      responses:
   *          200:
   *              description: 'Login Successful!'
   *          400:
   *              description: 'email account not found'
   *
   */
    public async login({ auth, request, response }: HttpContextContract){
        const payload = await request.validate(LoginValidator)
        const token = await auth.use('api').attempt(payload.email, payload.password)
        return response.ok({status: 'Login Successful!', data: token})
    }
  /**
   * @swagger
   * paths:
   *  /api/v1/verification:
   *    post:
   *      tags:
   *        - Authentication
   *      description: Setting isVerified variable in users table to True
   *      summary: Verification for new user through otp
   *      requestBody:
   *          required: true
   *          content:
   *              application/x-www-form-urlencoded:
   *                  schema:
   *                     type: object
   *                     properties:
   *                         email:
   *                           type: string
   *                         otp_code:
   *                           type: string
   *                     required:
   *                        - email
   *                        - otp_code
   *              application/json:
   *                  schema:
   *                     type: object
   *                     properties:
   *                         email:
   *                           type: string
   *                         otp_code:
   *                           type: string
   *                     required:
   *                        - email
   *                        - otp_code
   *      responses:
   *          200:
   *              description: 'Verification successful!'
   *          400:
   *              description: 'Not verified!'
   *
   */
    public async otp({ request, response }: HttpContextContract){
        const otp_code = request.input('otp_code')
        const email = request.input('email')
        const user = await User.findByOrFail('email', email)
        const data = await Database.from('otps').where('otp_code', otp_code).firstOrFail()
        if(user.id == data.user_id){
            user.isVerified = true
            await user.save()
            return response.ok({ status: 'Verification successful!', data: 'verified!'})
        } else {
            return response.badRequest({ status: 'error', data: 'Not verified!'})
        }
    }
}
