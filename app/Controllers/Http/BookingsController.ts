import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import BookingValidator from 'App/Validators/BookingValidator';
import Booking from 'App/Models/Booking';
import Field from 'App/Models/Field';
import Database from '@ioc:Adonis/Lucid/Database';
import User from "App/Models/User";

export default class BookingsController {
  /**
   * @swagger
   * paths:
   *  /api/v1/bookings:
   *    get:
   *      security:
   *         - bearerAuth: []
   *      tags:
   *        - Bookings
   *      summary: Get All Bookings Data
   *
   *      responses:
   *          200:
   *              description: 'Success'
   *          400:
   *              description: 'Failed'
   *          401:
   *              description: 'Unauthorized'
   *          500:
   *              description: 'Internal Server Error'
   *
   *
   */
    public async index({response}: HttpContextContract){
        const getBooking = await Booking.query()
      .select("id", "field_id", "play_date_start", "play_date_end", "user_id")
      .preload('field', (query) => {
        query.select("id", "name", "type");
      })
      .preload('bookingUser', (query) => {
        query.select("id", "name");
      })
      .preload("players", (query) => {
        query.select('id','name', 'email')
      });
    return response.ok({
      message: "success",
      status: "true",
      data: getBooking,
    });
  }
  /**
   * @swagger
   * paths:
   *  /api/v1/fields/{id}/bookings:
   *    post:
   *      security:
   *         - bearerAuth: []
   *      tags:
   *        - Venues
   *      summary: Create New Booking Venue Fields
   *      parameters:
   *        - in: path
   *          name: id
   *          required: true
   *          schema:
   *            type: number
   *            minimum: 1
   *          description: Venue ID
   *      requestBody:
   *          required: true
   *          content:
   *              application/x-www-form-urlencoded:
   *                  schema:
   *                      $ref: '#/definitions/Booking'
   *              application/json:
   *                  schema:
   *                      $ref: '#/definitions/Booking'
   *
   *      responses:
   *          200:
   *              description: 'Booking successfully made'
   *          400:
   *              description: 'Failed'
   *          401:
   *              description: 'Unauthorized'
   *          500:
   *              description: 'Internal Server Error'
   *
   *
   */
    public async store({ request, params, response, auth }: HttpContextContract){
        const field = await Field.findByOrFail('id', params.field_id)
        const payload = await request.validate(BookingValidator)
        const booking = new Booking()
        const user = auth.user!
        booking.playDateStart = payload.play_date_start
        booking.playDateEnd = payload.play_date_end
        booking.title = payload.title
        booking.related('field').associate(field)
        user.related('myBookings').save(booking)
        return response.created({ status: 'Booking successfully made', data: booking})
    }
  /**
   * @swagger
   * paths:
   *  /api/v1/bookings/{id}:
   *    get:
   *      security:
   *         - bearerAuth: []
   *      tags:
   *        - Bookings
   *      summary: Show Booking By Id
   *      parameters:
   *        - in: path
   *          name: id
   *          required: true
   *          schema:
   *            type: number
   *            minimum: 1
   *          description: Booking ID
   *      responses:
   *          200:
   *              description: 'Success'
   *          400:
   *              description: 'Failed'
   *          401:
   *              description: 'Unauthorized'
   *          500:
   *              description: 'Internal Server Error'
   *
   *
   */
    public async show({ params, response }: HttpContextContract){
        const booking = await Booking.query().where('id', params.id).preload('players', (userQuery) => {
            userQuery.select(['name', 'email', 'id'])
        }).withCount('players').firstOrFail()
        let raw = JSON.stringify(booking)
        let temp = {...JSON.parse(raw), players_count: booking.$extras.players_count}
        return response.ok({ status: 'success', data: temp})
    }
    /**
   * @swagger
   * paths:
   *  /api/v1/schedules:
   *    get:
   *      security:
   *         - bearerAuth: []
   *      tags:
   *        - Bookings
   *      summary: Get All Schedules
   *
   *      responses:
   *          200:
   *              description: 'Success'
   *          400:
   *              description: 'Failed'
   *          401:
   *              description: 'Unauthorized'
   *          500:
   *              description: 'Internal Server Error'
   *
   *
   */
    public async schedules({ response, auth }: HttpContextContract) {
        const user = auth.user;
        if(user){
        let booking = await User.query().preload('myBookings').where('id', user.id)
        if(booking){
        return response.ok({ message: 'success', data: booking })
        } else {
        return response.badRequest({ message: 'schedule not found' })
        }
        }
    }
    public async update({ request, response, params }: HttpContextContract){
        await Database.from('bookings').where('id', params.id).update({
             title: request.input('title'),
             play_date_start: request.input('play_date_start'),
             play_date_end: request.input('play_date_end')
        })
        return response.status(200).json({ message: 'data updated!'})
    }
    public async destroy({ params, response }: HttpContextContract){
        await Database.from('bookings').where('id', params.id).delete()
        return response.status(200).json({ message: 'data deleted!'}) 
    }
  /**
   * @swagger
   * paths:
   *  /api/v1/bookings/{id}/join:
   *    put:
   *      security:
   *         - bearerAuth: []
   *      tags:
   *        - Bookings
   *      summary: Join / Unjoin Booking
   *      parameters:
   *        - in: path
   *          name: id
   *          required: true
   *          schema:
   *            type: number
   *            minimum: 1
   *          description: Booking ID
   *      responses:
   *          200:
   *              description: 'Success'
   *          400:
   *              description: 'Failed'
   *          401:
   *              description: 'Unauthorized'
   *          500:
   *              description: 'Internal Server Error'
   *
   *
   */
    public async join ({ response, params, auth }: HttpContextContract){
        const booking = await Booking.findOrFail(params.id)
        let user = auth.user!
        const checking = await Database.from('schedules').where('booking_id', params.id).where('user_id', user.id).first()
        if(!checking){
            await booking.related('players').attach([user.id])
            return response.ok({ status: 'success', message: 'successfully join!'})
        } else {
            await booking.related('players').detach([user.id])
            return response.ok({ status: 'success', message: 'out of selected schedule'})
        }      
    } 
}
