import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import Database from '@ioc:Adonis/Lucid/Database';
import Venue from 'App/Models/Venue';
import VenueValidator from 'App/Validators/VenueValidator';

export default class VenuesController {
  /**
   * @swagger
   * paths:
   *  /api/v1/venues:
   *    get:
   *      security:
   *         - bearerAuth: []
   *      tags:
   *        - Venues
   *      summary: Collection of all venues data
   *
   *      responses:
   *          200:
   *              description: 'Data Venue Acquired!'
   *          400:
   *              description: 'Failed'
   *          401:
   *              description: 'Unauthorized'
   *          500:
   *              description: 'Internal Server Error'
   *
   *
   */
    public async index({ response }: HttpContextContract){
        let venues = await Database.from('venues').select('*')
        response.status(200).json({ message: 'Data Venue Acquired!', data: venues})
    }
  /**
   * @swagger
   * paths:
   *  /api/v1/venues:
   *    post:
   *      security:
   *         - bearerAuth: []
   *      tags:
   *        - Venues
   *      summary: Inserting new venue data
   *      requestBody:
   *          required: true
   *          content:
   *              application/x-www-form-urlencoded:
   *                  schema:
   *                      $ref: '#/definitions/Venue'
   *              application/json:
   *                  schema:
   *                      $ref: '#/definitions/Venue'
   *      responses:
   *          200:
   *              description: 'data stored!'
   *          400:
   *              description: 'Failed'
   *          401:
   *              description: 'Unauthorized'
   *          500:
   *              description: 'Internal Server Error'
   *
   *
   */
    public async store({ request, response }: HttpContextContract){
        try {
             await request.validate(VenueValidator);
             await Database.table('venues').insert({
             name: request.input('name'),
             address: request.input('address'),
             phone: request.input('phone'), 
          })
          response.created({message: 'data stored!'})
        } catch (error) {
             response.badRequest({errors: error.messages})
        }  
    }
  /**
   * @swagger
   * paths:
   *  /api/v1/venues/{id}:
   *    get:
   *      security:
   *         - bearerAuth: []
   *      tags:
   *        - Venues
   *      summary: Getting specific venue data by id
   *      parameters:
   *        - in: path
   *          name: id
   *          required: true
   *          schema:
   *            type: number
   *            minimum: 1
   *          description: Venue ID
   *      responses:
   *          200:
   *              description: 'id-filtered data acquired!'
   *          400:
   *              description: 'Failed'
   *          401:
   *              description: 'Unauthorized'
   *          500:
   *              description: 'Internal Server Error'
   *
   *
   */
    public async show ({ params, response}: HttpContextContract){
        let venues = await Venue.query().where('id', params.id).preload('fields').first()
        return response.status(200).json({ message: 'id-filtered data acquired!', data: venues})
    }
    public async update ({ request, response, params }: HttpContextContract){
        await Database.from('venues').where('id', params.id).update({
             name: request.input('name'),
             address: request.input('address'),
             phone: request.input('phone')
        })
        return response.status(200).json({ message: 'data updated!'})
    }
    public async destroy ({ params, response }: HttpContextContract){
        await Database.from('venues').where('id', params.id).delete()
        return response.status(200).json({ message: 'data deleted!'}) 
    }  
}
