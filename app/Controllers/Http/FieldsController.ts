import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import Field from 'App/Models/Field';
import Venue from 'App/Models/Venue';
import Database from '@ioc:Adonis/Lucid/Database';

export default class FieldsController {
    public async index ({ response }: HttpContextContract){
        let field = await Database.from('fields').select('*')
        response.status(200).json({ message: 'Data Fields Acquired!', data: field})
    }
  /**
   * @swagger
   * paths:
   *  /api/v1/venues/{id}/fields:
   *    post:
   *      security:
   *         - bearerAuth: []
   *      tags:
   *        - Venues
   *      summary: Creating new field data
   *      parameters:
   *        - in: path
   *          name: id
   *          required: true
   *          schema:
   *            type: number
   *            minimum: 1
   *          description: Venue ID
   *      requestBody:
   *          required: true
   *          content:
   *              application/x-www-form-urlencoded:
   *                  schema:
   *                      $ref: '#/definitions/Field'
   *              application/json:
   *                  schema:
   *                      $ref: '#/definitions/Field'
   *
   *      responses:
   *          200:
   *              description: 'Field added!'
   *          400:
   *              description: 'Failed'
   *          401:
   *              description: 'Unauthorized'
   *          500:
   *              description: 'Internal Server Error'
   *
   *
   */
    public async store ({ request, response, params }: HttpContextContract){
        const venue = await Venue.findByOrFail('id', params.venue_id)
        const newField = new Field()
        newField.name = request.input('name')
        newField.type = request.input('type')
        await newField.related('venue').associate(venue)
        return response.created({status: 'Field added!', data: newField})
    }
    public async show ({ params, response}: HttpContextContract){
        const field = await Field.query().where('id', params.id).preload('bookings', (bookingQuery) => {
            bookingQuery.select(['title', 'play_date_start', 'play_date_end'])
        }).firstOrFail()
        return response.ok({ status: 'Showing field data and bookings', data: field})
    }
    public async update ({ request, response, params }: HttpContextContract){
        await Database.from('fields').where('id', params.id).update({
             name: request.input('name'),
             type: request.input('type'),
        })
        return response.status(200).json({ message: 'data updated!'})
    }
    public async destroy ({ params, response }: HttpContextContract){
        await Database.from('fields').where('id', params.id).delete()
        return response.status(200).json({ message: 'data deleted!'}) 
    }
}
