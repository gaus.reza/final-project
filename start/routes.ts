/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return 'please refer to https://gausmainbersama.herokuapp.com/docs for testing, thank you'
})

// Route.group(() => {
//   Route.group(() => {
//     Route.post('/login', 'AuthController.login').as('Auth.login')
//     Route.post('/register', 'AuthController.register').as('Auth.register')
//     Route.post('verification', 'AuthController.otp').as('auth.verification')
//   })

//   Route.group(() => {
//     Route.resource('venues', 'VenuesController').apiOnly().middleware({'*': ['auth', 'acl:admin,venue_owner']});
//     Route.resource('venues.fields', 'FieldsController').apiOnly().middleware({'*': ['auth', 'acl:admin,venue_owner']});
//     Route.resource('venues.bookings', 'BookingsController').apiOnly().middleware({'*': ['auth', 'acl:admin,venue_owner']});

//     Route.get('bookings', 'BookingsController.index').as('Bookings.Index')
//     Route.get('bookings/:id', 'BookingsController.show').as('Bookings.Show')
//     Route.get('schedules', 'BookingsController.schedules').as('Bookings.Schedules')
//     Route.put('bookings/:id/join', 'BookingsController.join').as('Bookings.Join')
//     Route.put('bookings/:id/unjoin', 'BookingsController.unjoin').as('Bookings.Unjoin')

//   }).middleware('auth')

// }).prefix('api/v1');

Route.group(() => {
  Route.resource('venues', 'VenuesController').apiOnly().middleware({
    '*': ['auth', 'acl:admin,venue_owner']})
  Route.resource('venues.fields', 'FieldsController').apiOnly().middleware({
    '*': ['auth', 'acl:admin,venue_owner']})
  Route.resource('fields.bookings', 'BookingsController').apiOnly().middleware({
    '*': ['auth', 'acl:admin,user']})  
  Route.post('/login', 'AuthController.login').as('Auth.login')
  Route.post('/register', 'AuthController.register').as('Auth.register')
  Route.post('/verification', 'AuthController.otp').as('auth.verification')
  Route.get('/bookings', 'BookingsController.index').middleware(['auth', 'acl:admin,user']).as('booking.index')
  Route.get('/bookings/:id', 'BookingsController.show').middleware(['auth', 'acl:admin,user']).as('booking.show')
  Route.put('/bookings/:id/join', 'BookingsController.join').middleware(['auth', 'acl:admin,user']).as('booking.join')
  Route.get('/schedules', 'BookingsController.schedules').middleware(['auth', 'acl:admin,user']).as('booking.schedules')
}).prefix('/api/v1').as('apiv1')

// Route.get('/venue', 'VenuesController.index').as('Venues.index')
// Route.post('/venue', 'VenuesController.store').as('Venues.store')
// Route.get('/venue/:id', 'VenuesController.show').as('Venues.show')
// Route.put('/venue/:id', 'VenuesController.update').as('Venues.update')
// Route.delete('/venue/:id', 'VenuesController.delete').as('Venues.delete')
// Route.post('/login', 'AuthController.login').as('Auth.login')
// Route.post('/register', 'AuthController.register').as('Auth.register')
// Route.resource('venues.fields', 'FieldsController').apiOnly().middleware({
//   '*': ['auth']})

